<?php namespace Larefy\SoundWave\Components;

use Cms\Classes\ComponentBase;
use Larefy\SoundWave\Models\Album as SWAlbum;

class Album extends ComponentBase
{
    /**
     * @var Larefy\SoundWave\Models\Album The release model used for display.
     */
    public $album;

    public function componentDetails()
    {
        return [
            'name'        => 'larefy.soundwave::lang.components.album.details.name',
            'description' => 'larefy.soundwave::lang.components.album.details.description'
        ];
    }

    public function defineProperties()
    {
        return [
            'slug' => [
                'title'       => 'larefy.soundwave::lang.components.album.properties.slug.title',
                'description' => 'larefy.soundwave::lang.components.album.properties.slug.description',
                'default'     => '{{ :slug }}',
                'type'        => 'string'
            ]
        ];
    }

    public function onRun()
    {
        $this->album = $this->page['album'] = $this->loadAlbum();
    }

    public function onRender()
    {
        if (empty($this->album)) {
            $this->album = $this->page['album'] = $this->loadAlbum();
        }
    }

    protected function loadAlbum()
    {
        $slug = $this->property('slug');

        $albume = new SWAlbum;

        $albume = $albume->isClassExtendedWith('RainLab.Translate.Behaviors.TranslatableModel')
            ? $albume->transWhere('slug', $slug)
            : $albume->where('slug', $slug);

        $albume = $albume->isPublished();

        $albume = $albume->first();

        return $albume;
    }


}
