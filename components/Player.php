<?php namespace Larefy\SoundWave\Components;

use Cms\Classes\ComponentBase;
use Larefy\SoundWave\Models\Settings as SWSettings;

class Player extends ComponentBase
{
	/**
	 * URL to MusicFile component's page.
	 * @var string
	 */
	public $musicBaseURL;


    public function componentDetails()
    {
        return [
            'name'        => 'larefy.soundwave::lang.components.player.details.name',
            'description' => 'larefy.soundwave::lang.components.player.details.description'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
	    $this->prepareVars();
        $this->addJs('/plugins/larefy/soundwave/assets/dist/js/amplitudejs/dist/amplitude.min.js');
        $this->addJs('/plugins/larefy/soundwave/assets/dist/js/soundwave.js');
        $this->addCss('/plugins/larefy/soundwave/assets/dist/css/soundwave.css');
    }

	protected function prepareVars()
	{
		$this->musicBaseURL = $this->page['musicBaseURL'] = SWSettings::get('music_url', '/media/song/');
	}
}
