<?php namespace Larefy\SoundWave\Components;

use Cms\Classes\ComponentBase;
use Larefy\SoundWave\Models\Song;
use Response;
use Storage;
use Config;
use System\Classes\MediaLibrary;

class MusicFile extends ComponentBase
{
	public function componentDetails()
	{
		return [
			'name'        => 'larefy.soundwave::lang.components.music_file.details.name',
			'description' => 'larefy.soundwave::lang.components.music_file.details.description'
		];
	}

	public function defineProperties()
	{
		return [];
	}

	public function onRun()
	{
		$id = $this->param('id');
		$song = Song::findOrFail($id);
		$media = MediaLibrary::instance();
		$storageFolder = $media::validatePath(Config::get('cms.storage.media.folder', 'media'), true);
		$filename = $storageFolder . $song->url;
		$fileExist = Storage::disk('local')->has($filename);
		$mime_type = "audio/x-mpegurl";
		if ($fileExist) {
			$file = $media->get($song->url);
			$filesize = Storage::disk('local')->size($filename);
			$headers = [
				'Content-Description' => 'File Transfer',
				'Content-Type' =>$mime_type,
				'Content-Disposition' => "attachment; filename*=UTF-8''" . $song->url,
				'Content-Length' => $filesize,
				'Pragma' => 'public',
				'Cache-Control' => 'must-revalidate',
				'Expires' => '0',
			];
			return Response::make($file, 200, $headers);
		} else {
			return Response::make('File not find', 403);
		}
	}
}
