<?php namespace Larefy\SoundWave\Components;

use Redirect;
use Cms\Classes\Page;
use Cms\Classes\ComponentBase;
use Larefy\SoundWave\Models\Album as SWAlbum;

class Albums extends ComponentBase
{
    /**
     * A collection of albums to display
     * @var Collection
     */
    public $albums;

    /**
     * Parameter to use for the page number
     * @var string
     */
    public $pageParam;

    /**
     * Message to display when there are no messages.
     * @var string
     */
    public $noAlbumsMessage;

    /**
     * Reference to the page name for linking to albums.
     * @var string
     */
    public $albumPage;

    /**
     * If the release list should be ordered by another attribute.
     * @var string
     */
    public $sortOrder;

    public function componentDetails()
    {
        return [
            'name'        => 'larefy.soundwave::lang.components.albums.details.name',
            'description' => 'larefy.soundwave::lang.components.albums.details.description'
        ];
    }

    public function defineProperties()
    {
        return [
            'pageNumber' => [
                'title'       => 'larefy.soundwave::lang.components.albums.properties.pagination.title',
                'description' => 'larefy.soundwave::lang.components.albums.properties.pagination.description',
                'type'        => 'string',
                'default'     => '{{ :page }}',
            ],
            'albumsPerPage' => [
                'title'             => 'larefy.soundwave::lang.components.albums.properties.per_page.title',
                'type'              => 'string',
                'validationPattern' => '^[0-9]+$',
                'validationMessage' => 'larefy.soundwave::lang.components.albums.properties.per_page.validation',
                'default'           => '10',
            ],
            'noAlbumsMessage' => [
                'title'        => 'larefy.soundwave::lang.components.albums.properties.no_albums.title',
                'description'  => 'larefy.soundwave::lang.components.albums.properties.no_albums.description',
                'type'         => 'string',
                'default'      => 'No albums found',
                'showExternalParam' => false
            ],
            'sortOrder' => [
                'title'       => 'larefy.soundwave::lang.components.albums.properties.order.title',
                'description' => 'larefy.soundwave::lang.components.albums.properties.order.description',
                'type'        => 'dropdown',
                'default'     => 'published_at desc'
            ],
            'albumPage' => [
                'title'       => 'larefy.soundwave::lang.components.albums.properties.page.album.title',
                'description' => 'larefy.soundwave::lang.components.albums.properties.page.album.description',
                'type'        => 'dropdown',
                'default'     => 'albums/detail',
                'group'       => 'larefy.soundwave::lang.components.albums.properties.page.album.group',
            ],
        ];
    }

    public function getAlbumPageOptions()
    {
        return Page::sortBy('baseFileName')->lists('baseFileName', 'baseFileName');
    }

    public function getSortOrderOptions()
    {
        return SWAlbum::$allowedSortingOptions;
    }

    public function onRun()
    {
        $this->prepareVars();

        $this->albums = $this->page['albums'] = $this->listAlbums();
        /*
         * If the page number is not valid, redirect
         */
        if ($pageNumberParam = $this->paramName('pageNumber')) {
            $currentPage = $this->property('pageNumber');
            if ($currentPage > ($lastPage = $this->albums->lastPage()) && $currentPage > 1)
                return Redirect::to($this->currentPageUrl([$pageNumberParam => $lastPage]));
        }
    }

    protected function prepareVars()
    {
        $this->pageParam = $this->page['pageParam'] = $this->paramName('pageNumber');
        $this->noAlbumsMessage = $this->page['noAlbumsMessage'] = $this->property('noAlbumsMessage');
        $this->albumPage = $this->page['albumPage'] = $this->property('albumPage');
    }

    protected function listAlbums()
    {
        $albums= SWAlbum::isPublished()->listFrontEnd([
            'page'       => $this->property('pageNumber'),
            'sort'       => $this->property('sortOrder'),
            'perPage'    => $this->property('albumsPerPage'),
            'search'     => trim(input('search')),
        ]);

        $albums->each(function($album) {
            $album->setUrl($this->albumPage, $this->controller);
        });

        return $albums;
    }
}
