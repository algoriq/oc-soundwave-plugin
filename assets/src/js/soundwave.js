window.onkeydown = function(e) {
    return !(e.keyCode == 32);
};

$(document).ready(function(){

    // Prevent automatic playback when clicking on other links
    $('a').on('click', function(e){
        e.stopPropagation();
    });

});
	