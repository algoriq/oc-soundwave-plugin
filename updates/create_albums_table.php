<?php namespace Larefy\SoundWave\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateAlbumsTable extends Migration
{
    public function up()
    {
        Schema::create('larefy_soundwave_albums', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('artist')->nullable();
            $table->string('title')->nullable();
            $table->string('slug')->nullable()->index();
            $table->string('label')->nullable();
            $table->json('available_links')->nullable();
            $table->text('excerpt')->nullable();
            $table->string('cover_art_url')->nullable();
            $table->boolean('published')->default(false);
            $table->timestamp('released_at')->nullable();
            $table->timestamp('published_at')->nullable();
            $table->timestamps();
        });

        Schema::create('larefy_soundwave_albums_songs', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('album_id')->unsigned();
            $table->integer('song_id')->unsigned();
            $table->primary(['album_id', 'song_id'], 'album_song');
        });
    }

    public function down()
    {
        Schema::dropIfExists('larefy_soundwave_albums');
        Schema::dropIfExists('larefy_soundwave_albums_songs');
    }
}
