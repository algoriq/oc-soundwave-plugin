<?php namespace Larefy\SoundWave\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateSongsTable extends Migration
{
    public function up()
    {
        Schema::create('larefy_soundwave_songs', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('artist')->nullable();
            $table->string('name')->nullable();
            $table->string('url')->nullable();
            $table->json('available_links')->nullable();
            $table->string('cover_art_url')->nullable();
            $table->boolean('downloadable')->default(false);
            $table->boolean('published')->default(false);
            $table->timestamp('published_at')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('larefy_soundwave_songs');
    }
}
