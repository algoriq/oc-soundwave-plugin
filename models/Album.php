<?php namespace Larefy\SoundWave\Models;

use Db;
use Lang;
use Model;
use ValidationException;
use Carbon\Carbon;
use Cms\Classes\Page as CmsPage;

/**
 * Album Model
 */
class Album extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'larefy_soundwave_albums';

    /**
     * @var array Attributes that support validation, if available.
     */
    public $rules = [
        'artist' => 'required',
        'title' => 'required',
        'slug' => ['required', 'regex:/^[a-z0-9\/\:_\-\*\[\]\+\?\|]*$/i', 'unique:larefy_soundwave_albums'],
        'excerpt' => ''
    ];

    /**
     * @var array Attributes that support translation, if available.
     */
    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];

    public $translatable = [
        'artist',
        'title',
        'excerpt',
        ['slug', 'index' => true]
    ];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Fillable fields
     */
    protected $jsonable = [
        'available_links'
    ];

    /**
     * The attributes that should be mutated to dates.
     * @var array
     */
    protected $dates = ['published_at'];

    /**
     * The attributes on which the post list can be ordered
     * @var array
     */
    public static $allowedSortingOptions = [
        'title asc' => 'Title (ascending)',
        'title desc' => 'Title (descending)',
        'created_at asc' => 'Created (ascending)',
        'created_at desc' => 'Created (descending)',
        'updated_at asc' => 'Updated (ascending)',
        'updated_at desc' => 'Updated (descending)',
        'published_at asc' => 'Published (ascending)',
        'published_at desc' => 'Published (descending)',
        'random' => 'Random'
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [
        'songs' => [
            'Larefy\SoundWave\Models\Song',
            'table' => 'larefy_soundwave_albums_songs',
            'order' => 'created_at desc'
        ]
    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public function getSourceOptions($value, $formData)
    {
        return [
            'beatport' => 'Beatport',
            'soundcloud' => 'Soundcloud',
            'spotify' => 'Spotify',
            'itunes' => 'iTunes',
        ];
    }

    public function afterValidate()
    {
        if ($this->published && !$this->published_at) {
            throw new ValidationException([
                'published_at' => Lang::get('larefy.soundwave::lang.models.all.fields.published_at.validation')
            ]);
        }
    }

    /**
     * Sets the "url" attribute with a URL to this object
     * @param string $pageName
     * @param $controller
     */
    public function setUrl($pageName, $controller)
    {
        $params = [
            'id'   => $this->id,
            'slug' => $this->slug,
        ];

        //expose published year, month and day as URL parameters
        if ($this->published) {
            $params['year'] = $this->published_at->format('Y');
            $params['month'] = $this->published_at->format('m');
            $params['day'] = $this->published_at->format('d');
        }

        return $this->url = $controller->pageUrl($pageName, $params);
    }

    public function scopeIsPublished($query)
    {
        return $query
            ->whereNotNull('published')
            ->where('published', true)
            ->whereNotNull('published_at')
            ->where('published_at', '<', Carbon::now())
            ;
    }

    /**
     * Lists albums for the front end
     *
     * @param        $query
     * @param  array $options Display options
     *
     * @return Album
     */
    public function scopeListFrontEnd($query, $options)
    {
        /*
         * Default options
         */
        extract(array_merge([
            'page'       => 1,
            'perPage'    => 30,
            'sort'       => 'created_at',
            'search'     => '',
            'published'  => true,
        ], $options));

        $searchableFields = ['title', 'slug', 'excerpt'];

        if ($published) {
            $query->isPublished();
        }

        /*
         * Sorting
         */
        if (!is_array($sort)) {
            $sort = [$sort];
        }

        foreach ($sort as $_sort) {

            if (in_array($_sort, array_keys(self::$allowedSortingOptions))) {
                $parts = explode(' ', $_sort);
                if (count($parts) < 2) {
                    array_push($parts, 'desc');
                }
                list($sortField, $sortDirection) = $parts;
                if ($sortField == 'random') {
                    $sortField = Db::raw('RAND()');
                }
                $query->orderBy($sortField, $sortDirection);
            }
        }

        /*
         * Search
         */
        $search = trim($search);
        if (strlen($search)) {
            $query->searchWhere($search, $searchableFields);
        }

        return $query->paginate($perPage, $page);
    }

    /**
     * Returns URL of a album page.
     *
     * @param $pageCode
     * @param $theme
     */
    protected static function getAlbumPageUrl($pageCode, $theme)
    {
        $page = CmsPage::loadCached($theme, $pageCode);
        if (!$page) return;

        $properties = $page->getComponentProperties('soundwaveAlbum');
        if (!isset($properties['slug'])) {
            return;
        }
        $url = CmsPage::url($page->getBaseFileName());

        return $url;
    }

}
