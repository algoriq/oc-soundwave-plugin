<?php namespace Larefy\SoundWave\Models;

use Model;

class Settings extends Model
{
	public $implement = ['System.Behaviors.SettingsModel'];

	// A unique code
	public $settingsCode = 'larefy_sw_settings';

	// Reference to field configuration
	public $settingsFields = 'fields.yaml';

	public function initSettingsData()
	{
		$this->music_url = '/media/song/';
	}
}