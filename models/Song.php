<?php namespace Larefy\SoundWave\Models;

use Lang;
use Model;
use ValidationException;

/**
 * Song Model
 */
class Song extends Model
{
    use \October\Rain\Database\Traits\Validation;
    /**
     * @var string The database table used by the model.
     */
    public $table = 'larefy_soundwave_songs';

    /**
     * @var array Attributes that support validation, if available.
     */
    public $rules = [
        'artist' => 'required',
        'name' => 'required',
    ];

    /**
     * @var array Attributes that support translation, if available.
     */
    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];

    public $translatable = [
        'artist',
        'name',
    ];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Fillable fields
     */
    protected $jsonable = [
        'available_links'
    ];

    /**
     * The attributes that should be mutated to dates.
     * @var array
     */
    protected $dates = ['published_at'];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [
        'albums' => [
            'Larefy\SoundWave\Models\Album',
            'table' => 'larefy_soundwave_albums_songs',
            'order' => 'title'
        ]
    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public function getSourceOptions($value, $formData)
    {
        return [
            'beatport' => 'Beatport',
            'soundcloud' => 'Soundcloud',
            'spotify' => 'Spotify',
            'itunes' => 'iTunes',
        ];
    }

    public function afterValidate()
    {
        if ($this->published && !$this->published_at) {
            throw new ValidationException([
                'published_at' => Lang::get('larefy.soundwave::lang.models.all.fields.published_at.validation')
            ]);
        }
    }
}
