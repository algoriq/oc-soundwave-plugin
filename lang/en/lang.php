<?php return [
    'plugin' => [
        'details' => [
            'name' => 'Sound Wave',
            'description' => 'Create your own song albums using this plugin.',
            'author' => 'Larefy'
        ],
        'permissions' => [
            'access_albums' => 'Manage Albums',
            'access_songs' => 'Manage Songs',
        ],
        'navigations' => [
            'albums' => 'Albums',
            'songs' => 'Songs',
        ],
    ],
    'components' => [
        'album' => [
            'details' => [
                'name'        => 'Album',
                'description' => 'Displays a album on the page.'
            ],
            'properties' => [
                'slug' => [
                    'title' => 'Slug',
                    'description' => 'Look up the album using the supplied slug value.'
                ]
            ],
        ],
        'albums' => [
            'details' => [
                'name'        => 'Albums',
                'description' => 'Displays a list of latest albums on the page.'
            ],
            'properties' => [
                'pagination' => [
                    'title' => 'Page number',
                    'description' => 'This value is used to determine what page the user is on.'
                ],
                'per_page' => [
                    'title' => 'Per page',
                    'validation' => 'Invalid format of the albums per page value'
                ],
                'no_albums' => [
                    'title' => 'No albums message',
                    'description' => 'Message to display in the album list in case if there are no albums. This property is used by the default component partial.'
                ],
                'order' => [
                    'title' => 'Sort order',
                    'description' => 'Attribute on which the albums should be ordered'
                ],
                'page' => [
                    'album' => [
                        'title' => 'Album page',
                        'description' => 'Name of the album page file for the "Learn more" links.',
                        'group' => 'Links',
                    ],
                ],
            ]
        ],

        'player' => [
            'details' => [
                'name'        => 'Player',
                'description' => 'Displays a player on the page.'
            ],
        ],
	    'music_file' => [
		    'details' => [
			    'name'        => 'Music File',
			    'description' => 'Protect files from external access'
		    ],
		    'properties' => [
			    'site_ip' => [
				    'title' => 'Site ip',
				    'description' => 'Site ip from where music file can be accessed'
			    ],
		    ],
	    ],
    ],
    'models' => [
        'all' => [
            'columns' => [
                'created_at' => [
                    'label' => 'Created'
                ],
                'updated_at' => [
                    'label' => 'Updated'
                ],
                'released_at' => [
                    'label' => 'Released'
                ],
                'published_at' => [
                    'label' => 'Published',
                ]
            ],
            'fields' => [
                'tabs' => [
                    'manage' => 'Manage',
                    'albums' => 'Albums',
                    'songs' => 'Songs',
                    'available' => 'Available'
                ],
                'published' => [
                    'label' => 'Published',
                ],
                'published_at' => [
                    'label' => 'Published On',
                    'validation' => 'Please specify the published date',
                ],
                'available_links' => [
                    'label' => 'Available on',
                    'prompt' => 'Add new available link',
                    'fields' => [
                        'source' => [
                            'label' => 'Source'
                        ],
                        'source_link' => [
                            'label' => 'Url'
                        ]
                    ]
                ],
            ],
        ],
        'album' => [
            'columns' => [
                'artist' => [
                    'label' => 'Artist'
                ],
                'title' => [
                    'label' => 'Title',
                ],
            ],
            'fields' => [
                'artist' => [
                    'label' => 'Artist'
                ],
                'title' => [
                    'label' => 'Title',
                ],
                'slug' => [
                    'label' => 'Slug'
                ],
                'songs' => [
                    'label' => 'Songs'
                ],
                'cover_art_url' => [
                    'label' => 'Cover'
                ],
                'label' => [
                    'label' => 'Label'
                ],
                'excerpt' => [
                    'label' => 'Excerpt'
                ],
                'released_at' => [
                    'label' => 'Release date'
                ]
            ],
        ],
        'song' => [
            'columns' => [
                'artist' => [
                    'label' => 'Artist'
                ],
                'name' => [
                    'label' => 'Name'
                ],
            ],
            'fields' => [
                'artist' => [
                    'label' => 'Artist'
                ],
                'name' => [
                    'label' => 'Name'
                ],
                'downloadable' => [
                    'label' => 'Downloadable?'
                ],
                'url' => [
                    'label' => 'Song File'
                ],
                'cover_art_url' => [
                    'label' => 'Cover'
                ],
            ],
        ],
	    'settings' => [
		    'fields' => [
			    'music_url' => [
				    'label' => 'URL where page with MusicFile component located'
			    ],
		    ],
	    ]
    ],
	'settings' => [
		'menu_label' => 'Sound Wave settings',
		'menu_description' => 'Settings for Sound Wave plugin'
	]
];