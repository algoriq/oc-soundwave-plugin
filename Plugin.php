<?php namespace Larefy\SoundWave;

use Backend;
use System\Classes\PluginBase;

/**
 * SoundWave Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'larefy.soundwave::lang.plugin.details.name',
            'description' => 'larefy.soundwave::lang.plugin.details.description',
            'author'      => 'larefy.soundwave::lang.plugin.details.author',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            'Larefy\SoundWave\Components\Album'     => 'soundwaveAlbum',
            'Larefy\SoundWave\Components\Albums'    => 'soundwaveAlbums',
            'Larefy\SoundWave\Components\Player'    => 'soundwavePlayer',
            'Larefy\SoundWave\Components\MusicFile'    => 'soundwaveMusicFile',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return [
            'larefy.soundwave.access_albums' => [
                'tab' => 'larefy.soundwave::lang.plugin.details.name',
                'label' => 'larefy.soundwave::lang.plugin.permissons.access_albums'
            ],
            'larefy.soundwave.access_songs' => [
                'tab' => 'larefy.soundwave::lang.plugin.details.name',
                'label' => 'larefy.soundwave::lang.plugin.permissons.access_songs'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [
            'soundwave' => [
                'label'       => 'larefy.soundwave::lang.plugin.details.name',
                'url'         => Backend::url('larefy/soundwave/albums'),
                'icon'        => 'icon-leaf',
                'iconSvg'     => 'plugins/larefy/soundwave/assets/dist/images/sound-wave.svg',
                'permissions' => ['larefy.soundwave.access_albums'],
                'order'       => 500,

                'sideMenu' => [
                    'albums' => [
                        'label'       => 'larefy.soundwave::lang.plugin.navigations.albums',
                        'icon'        => 'icon-copy',
                        'url'         => Backend::url('larefy/soundwave/albums'),
                        'permissions' => ['larefy.soundwave.access_albums']
                    ],
                    'songs' => [
                        'label'       => 'larefy.soundwave::lang.plugin.navigations.songs',
                        'icon'        => 'icon-list-ul',
                        'url'         => Backend::url('larefy/soundwave/songs'),
                        'permissions' => ['larefy.soundwave.access_songs']
                    ]
                ]
            ],
        ];
    }

	public function registerSettings()
	{
		return [
			'settings' => [
				'label'       => 'larefy.soundwave::lang.settings.menu_label',
				'description' => 'larefy.soundwave::lang.settings.menu_description',
				'category'    => 'Sound Wave',
				'icon'        => 'icon-cog',
				'class'       => 'Larefy\SoundWave\Models\Settings',
				'order'       => 500
			]
		];
	}
}
