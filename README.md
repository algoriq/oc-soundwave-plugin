## Sound Wave
##### Create your own song albums using this plugin. It also allows users to play and download your songs from the album.

## Implementing front-end pages

##### Album List Page

Use the `soundwaveAlbums` component to display a list of latest albums on a page. The component has the following properties:

* **pageNumber** - this value is used to determine what page the user is on, it should be a routing parameter for the default markup. The default value is **{{ :page }}** to obtain the value from the route parameter `:page`.
* **albumsPerPage** - how many albums to display on a single page (the pagination is supported automatically). The default value is 10.
* **noAlbumsMessage** - message to display in the empty album list.
* **sortOrder** - the column name and direction used for the sort order of the albums. The default value is **published_at desc**.
* **albumPage** - path to the album details page. The default value is **albums/detail** - it matches the pages/albums/detail.htm file in the theme directory. This property is used in the default component partial for creating links to the albums.

The `soundwaveAlbums` component injects the following variables to the page where it's used:

* **albums** - a list of Sound Wave Albums loaded from the database.
* **albumPage** - contains the value of the `albumPage` component's property.
* **noAlbumsMessage** - contains the value of the `noAlbumsMessage` component's property.

The component supports pagination and reads the current page index from the `:page` URL parameter. The next example shows the basic component usage on the album home page:

    title = "Albums"
    url = "/albums/:page?"
    layout = "default"
    is_hidden = 0

    [soundwaveAlbums]
    pageNumber = "{{ :page }}"
    albumsPerPage = 10
    noAlbumsMessage = "No albums found"
    sortOrder = "published_at desc"
    albumPage = "albums/detail"
    ==
    {% component 'soundwaveAlbums' %}

The album list and the pagination are coded in the default component partial `plugins/larefy/soundwave/components/albums/default.htm`. If the default markup is not suitable for your website, feel free to copy it from the default partial and replace the `{% component %}` call in the example above with the partial contents.

##### Album Single Page

Use the `soundwaveAlbum` component to display a album on a page. The component has the following properties:

* **slug** - the value used for looking up the album by its slug. The default value is **{{ :slug }}** to obtain the value from the route parameter `:slug`.

The component injects the following variables to the page where it's used:

* **album** - the album object loaded from the database. If the album is not found, the variable value is **null**.

The next example shows the basic component usage on the album page:

    title = "Album"
    url = "/album/:slug"
    layout = "default"
    is_hidden = 0

    [soundwaveAlbum]
    slug = "{{ :slug }}"
    ==
    {% component 'soundwaveAlbum' %}

The album details is coded in the default component partial `plugins/larefy/soundwave/components/album/default.htm`.

##### Player
Use the `soundwavePlayer` component to display a player on a layout or single page.

The player contains several standard commands like skipbackward, play-pause, skipforward, shuffle, repeat

##### Music File
Use the `soundwaveMusicFile` component to link song file with ***media library***.

    title = "Music File"
    url = "/media/song/:id?"
    layout = "default"
    is_hidden = 0

    [soundwaveMusicFile]
    ==
    {% component 'soundwaveMusicFile' %}

Then specify the path to the song in the plugin's settings, by default it's `/media/song/`, you can use your parameter, the main thing is that it matches the component's page and settings.

Warning!!! Without this item, playing songs in the player will be impossible.

## Default required assets

All components support the default [Bootstrap 4](http://getbootstrap.com/) and [Ionicons](http://ionicons.com/), if the default markup is not suitable for your website, feel free to copy it from the default partial and replace the `{% component %}` call in the example above with the partial contents.

## Reporting Issues
If you are experiencing any issues or if you have a feature request, please [open up a new Bitbucket Issue](https://bitbucket.org/getlarefy/oc-soundwave-plugin/issues?status=new&status=open)